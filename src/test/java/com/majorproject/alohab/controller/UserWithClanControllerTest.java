package com.majorproject.alohab.controller;

import com.majorproject.alohab.AlohaBApplication;
import com.majorproject.alohab.dto.model.UserWithClan;
import com.majorproject.alohab.dto.repository.UserWithClanRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = AlohaBApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(
        locations = "classpath:application-integration's.properties")
public class UserWithClanControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserWithClanController userWithClanController;

    @Autowired
    private UserWithClanRepository userWithClanRepository;

    public List<UserWithClan> testingData() {
        List<UserWithClan> userWithClanList = new ArrayList<>();
        userWithClanList.add(new UserWithClan(1, 1, "test", "test", "test"));
        userWithClanList.add(new UserWithClan(1, 2, "test", "test", "test"));
        userWithClanList.add(new UserWithClan(1, 3, "test", "test", "test"));
        return userWithClanList;
    }

    public List<UserWithClan> testingDataMultipleClans(){
        List<UserWithClan> userWithClanList = new ArrayList<>();
        userWithClanList.addAll(testingData());
        userWithClanList.add(new UserWithClan(2, 4, "test", "test", "test"));
        userWithClanList.add(new UserWithClan(2, 5, "test", "test", "test"));
        userWithClanList.add(new UserWithClan(2, 6, "test", "test", "test"));
        return userWithClanList;
    }

    @BeforeEach
    public void delete_all_from_database(){
        userWithClanRepository.deleteAll();
    }

    @Test
    public void adding_user_with_clan() {
        userWithClanController.addUserWithClan(testingData());
        assert testingData().equals(userWithClanRepository.findAll());
    }

    @Test
    public void get_users_by_clan_id(){
        userWithClanRepository.saveAll(testingDataMultipleClans());
        assert testingData().equals(userWithClanController.clanMembers(1));
    }
}