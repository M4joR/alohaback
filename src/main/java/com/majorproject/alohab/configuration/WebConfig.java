package com.majorproject.alohab.configuration;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${frontUrl}")
    private String frontEndURL;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(frontEndURL)
                .allowedMethods("HEAD", "GET", "PUT", "POST", "DELETE", "PATCH", "REQUEST")
                .allowCredentials(true);
    }
}
