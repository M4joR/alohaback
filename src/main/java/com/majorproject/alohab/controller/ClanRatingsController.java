package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.ClanRating;
import com.majorproject.alohab.service.ClanRatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/basicClan")
public class ClanRatingsController {

    private final ClanRatingService clanRatingService;

    @GetMapping
    public  List<ClanRating> allRatingsOrderByDate(){
        return clanRatingService.getRatings();
    }
}
