package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.Absence;
import com.majorproject.alohab.dto.request.AbsenceRequest;
import com.majorproject.alohab.service.AbsenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/absence")
public class AbsenceController {

    private final AbsenceService absenceService;

    @GetMapping
    public List<Absence> getAllAbsence(){
        return absenceService.findAllAbsence();
    }

    @PostMapping
    public void saveAbsence(@RequestBody AbsenceRequest absenceRequest){
        absenceService.addAbsence(absenceRequest);
    }

    @DeleteMapping("{absenceId}")
    public void deleteAbsence(@PathVariable Long absenceId){absenceService.deleteAbsence(absenceId);}
}
