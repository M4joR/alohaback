package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.Province;
import com.majorproject.alohab.service.ProvinceServices;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("provinces")
public class ProvinceController {

    private final ProvinceServices provinceServices;

    @GetMapping
    public  List<Province> getBasicClanProvinces(){
        return provinceServices.getBasicClanProvinces();
    }

    @GetMapping("/clanBattles")
    public  List<Province> clanBattles(){
        return provinceServices.getClanBattles();
    }

    @GetMapping("/clanDefends")
    public  List<Province> getClanDefends(){
        return provinceServices.getClanDefends();
    }
}
