package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.DeletedUserNotification;
import com.majorproject.alohab.service.DeletedUserNotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("notifications")
public class DeletedUserNotificationController {

    private final DeletedUserNotificationService deletedUserNotificationService;

    @GetMapping
    public  List<DeletedUserNotification> getAllNotifications(){
        return deletedUserNotificationService.getAllNotifications();
    }
}
