package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.request.AllUsersFromClanRequest;
import com.majorproject.alohab.dto.model.UserWithClan;
import com.majorproject.alohab.service.UserWithClanService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/userWithClan")
public class UserWithClanController {

    private final UserWithClanService userWithClanService;

    @PostMapping("addUserWithClan")
    public  List<UserWithClan> addUserWithClan(@RequestBody List<UserWithClan> requests){
        return userWithClanService.addUserWithClanList(requests);
    }

    @GetMapping("clanMembers/{clanId}")
    public List<UserWithClan> clanMembers(@PathVariable Integer clanId){
        return userWithClanService.allMembersByClanId(clanId);
    }

    @PostMapping("deletedUsers")
    public  List<UserWithClan> deletedUsers(@RequestBody AllUsersFromClanRequest request){
        return userWithClanService.allDeletedUsersByClanId(request.getClanId(), request.getUserId());
    }

    @DeleteMapping("deleteUsers/{clanId}")
    public  List<UserWithClan> deleteAllUsersByClanId(@PathVariable List<Integer> clanId){
        return userWithClanService.deleteAllUsersByClanId(clanId);
    }
}
