package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.Clan;
import com.majorproject.alohab.service.ClanService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clan")
public class ClanController {

    private final ClanService clanService;

    @GetMapping
    public List<Clan> getClans(){
        return clanService.getClans();
    }

    @PutMapping("addClan")
    public Clan addClan(@RequestBody Clan clan) {
        return clanService.addClan(clan);
    }

    @PutMapping
    public List<Clan> addClanList(@RequestBody List<Clan> clanlist) {return  clanService.addClanList(clanlist); }

    @DeleteMapping("deleteClan/{clanId}")
    public void deleteClan(@PathVariable Integer clanId){
        clanService.deleteClan(clanId);
    }
}
