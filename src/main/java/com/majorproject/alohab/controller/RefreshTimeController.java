package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.RefreshTime;
import com.majorproject.alohab.service.RefreshTimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("refreshTime")
public class RefreshTimeController {

    private final RefreshTimeService refreshTimeService;

    @GetMapping
    public List<RefreshTime> getAllTimes(){
        return refreshTimeService.getAllTimes();
    }

}
