package com.majorproject.alohab.controller;

import com.majorproject.alohab.dto.model.DeletedUser;
import com.majorproject.alohab.service.DeletedUserNotificationService;
import com.majorproject.alohab.service.DeletedUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/deletedUsers")
public class DeletedUserController {

    private final DeletedUserService deletedUserService;

    private final DeletedUserNotificationService deletedUserNotificationService;

    @PostMapping("addDeletedUsers")
    public  List<DeletedUser> addDeletedUsers(@RequestBody List<DeletedUser> requests){
        return deletedUserService.addDeletedUserList(requests);
    }

    @GetMapping("allDeletedUsers")
    public  List<DeletedUser> allDeletedUsers(){
        return deletedUserService.getAllDeletedUsers();
    }

    @DeleteMapping("reject/{userId}/{owner}")
    public  void rejectDeleteUser(
            @PathVariable Integer userId,
            @PathVariable String owner
    ){
        deletedUserNotificationService.addNotification(deletedUserService.getUserNameByUserId(userId), false, owner);
        deletedUserService.deleteUser(userId);
    }

    @DeleteMapping("accept/{userId}/{owner}")
    public  void acceptDeleteUser(
            @PathVariable Integer userId,
            @PathVariable String owner
    ){
        deletedUserNotificationService.addNotification(deletedUserService.getUserNameByUserId(userId), true, owner);
        deletedUserService.deleteUser(userId);
    }
}
