package com.majorproject.alohab.controller.wargaming;

import com.majorproject.alohab.dto.model.ClanMember;
import com.majorproject.alohab.service.ClanMemberService;
import com.majorproject.alohab.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/login")
public class WargamingLoginController {

    @Value("${frontUrl}")
    private String frontUrl;

    private final ClanMemberService clanMemberService;

    private final JwtService jwtService;

    @RequestMapping(method = RequestMethod.GET)
    public void wargamingLogin(HttpServletResponse response,
                                String status,
                                String access_token,
                                Integer expires_at,
                                Integer account_id,
                                String nickname) throws IOException {

        String userRole = "notInClan";
        String userRoleName = "Poza Klanem";

        if (status.equals("ok")){
            if(clanMemberService.findById(account_id) != null){
                ClanMember clanMember = clanMemberService.findById(account_id);
                userRole = clanMember.getUserRole();
                userRoleName= clanMember.getUserRoleName();
            }

            response.addCookie(new Cookie("ALOHA_TOKEN",
                            jwtService.generateToken(nickname, access_token, account_id, expires_at, userRole, userRoleName, getRoleId(userRole))));
            response.sendRedirect(frontUrl);
        }
    }

    private Integer getRoleId(String userRole) {

        switch (userRole) {
            case "reservist":
                return 1;
            case "recruit":
                return 2;
            case "private":
                return 3;
            case "junior_officer":
                return 4;
            case "quartermaster":
                return 5;
            case "intelligence_officer":
                return 6;
            case "recruitment_officer":
                return 7;
            case "combat_officer":
                return 8;
            case "personnel_officer":
                return 9;
            case "executive_officer":
                return 10;
            case "commander":
                return 11;
            default:
                return 0;
        }
    }
}
