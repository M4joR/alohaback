package com.majorproject.alohab.dto.wargaming.globalmap;

import com.majorproject.alohab.dto.wargaming.DataModel;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProvinceDataModel extends DataModel {
    List<Provinces> data;
}
