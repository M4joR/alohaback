package com.majorproject.alohab.dto.wargaming.members;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Member {
    String account_name;
    Integer account_id;
    String role_i18n;
    String role;
}
