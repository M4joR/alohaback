package com.majorproject.alohab.dto.wargaming.members;

import lombok.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Members {
    List<Member> members;
}
