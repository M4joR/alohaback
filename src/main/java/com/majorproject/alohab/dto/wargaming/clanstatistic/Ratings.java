package com.majorproject.alohab.dto.wargaming.clanstatistic;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Ratings {
    Integer rank_delta;
    Integer rank;
    Integer value;
}
