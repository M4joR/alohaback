package com.majorproject.alohab.dto.wargaming.globalmap;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FrontDetails {
    String front_name;
    Integer provinces_count;
    String front_id;
}
