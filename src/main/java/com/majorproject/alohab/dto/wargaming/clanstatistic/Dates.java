package com.majorproject.alohab.dto.wargaming.clanstatistic;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Dates {
    List<Integer> dates;
}
