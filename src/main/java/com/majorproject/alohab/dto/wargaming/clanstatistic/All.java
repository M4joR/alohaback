package com.majorproject.alohab.dto.wargaming.clanstatistic;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class All {
    Dates all;
}
