package com.majorproject.alohab.dto.wargaming;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataModel {
    String status;
    Meta meta;

}
