package com.majorproject.alohab.dto.wargaming.clanstatistic;

import com.majorproject.alohab.dto.wargaming.DataModel;
import lombok.*;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StatisticDetailsDataModel extends DataModel {
    Map<Integer, AdvancedEloRating> data;
}
