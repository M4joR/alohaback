package com.majorproject.alohab.dto.wargaming.members;


import com.majorproject.alohab.dto.wargaming.DataModel;
import lombok.*;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class MembersDataModel extends DataModel {
    Map<Integer, Members> data;
}
