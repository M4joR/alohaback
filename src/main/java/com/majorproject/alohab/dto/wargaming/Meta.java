package com.majorproject.alohab.dto.wargaming;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Meta {
    Integer count;
    Integer page_total;
    Integer total;
    Integer limit;
    Integer page;
}
