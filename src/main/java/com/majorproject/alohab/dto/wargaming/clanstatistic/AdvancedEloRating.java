package com.majorproject.alohab.dto.wargaming.clanstatistic;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AdvancedEloRating {
    Ratings fb_elo_rating_10;
}
