package com.majorproject.alohab.dto.wargaming.globalmap;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Provinces {
    Integer owner_clan_id;
    String arena_name;
    String arena_id;
    String province_id;
    String province_name;
    String prime_time;
    Integer daily_revenue;
    String uri;
    List<Integer> attackers;
    List<Integer> competitors;
    Integer round_number;
}
