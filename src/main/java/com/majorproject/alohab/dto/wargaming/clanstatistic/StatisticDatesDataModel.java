package com.majorproject.alohab.dto.wargaming.clanstatistic;

import com.majorproject.alohab.dto.wargaming.DataModel;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StatisticDatesDataModel extends DataModel {
    All data;
}
