package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.List;

@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
@Table
@Entity
@Data
@ToString
public class Province {
    @Id
    @NonNull
    String provinceId;

    String arenaName;

    String arenaId;

    String provinceName;

    Integer ownerClanId;

    @NonNull
    LocalTime primeTime;

    Integer dailyRevenue;

    String uri;

    String frontId;

    Integer roundNumber;

    @ElementCollection( targetClass = Integer.class )
    List<Integer> attacker;

    @ElementCollection( targetClass = Integer.class )
    List<Integer> competitor;
}
