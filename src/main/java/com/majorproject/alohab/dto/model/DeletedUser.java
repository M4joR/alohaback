package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class DeletedUser {

    @Id
    @NonNull
    private Integer userId;

    @NonNull
    private String userName;

    @NonNull
    private String userRoleName;

    @NonNull
    private LocalDate date;

    @NonNull
    private Integer clanId;
}
