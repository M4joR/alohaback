package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class ClanMember {

    @Id
    @NonNull
    private Integer userId;

    @NonNull
    private String userName;

    @NonNull
    private String userRoleName;

    @NonNull
    private String userRole;
}
