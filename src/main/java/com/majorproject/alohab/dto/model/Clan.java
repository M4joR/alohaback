package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;

@RequiredArgsConstructor
@NoArgsConstructor
@Table
@Entity
@Data
@ToString
public class Clan {

    @Id
    @NonNull
    private Integer clanId;

    @NonNull
    private String clanTag;

    @NonNull
    private String clanName;

    @NonNull
    private String clanEmblem;

}
