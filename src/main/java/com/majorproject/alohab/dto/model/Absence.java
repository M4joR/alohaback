package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@RequiredArgsConstructor
@NoArgsConstructor
@Table
@Entity
@Data
@AllArgsConstructor
@ToString
public class Absence {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long absenceId;

    @ManyToOne(fetch = FetchType.EAGER)
    @NonNull
    private ClanMember clanMember;

    @NonNull
    private LocalDate timeOfSend;

    @NonNull
    private LocalDate startAbsence;

    @NonNull
    private LocalDate endAbsence;

    @NonNull
    private String description;
}
