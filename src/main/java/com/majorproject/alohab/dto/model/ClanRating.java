package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class ClanRating {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Long clanRatingId;

    @NonNull
    LocalDate date;

    @NonNull
    Integer rank;

    @NonNull
    Integer rankDelta;

    @NonNull
    Integer value;
}
