package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class UserWithClan {

    @NonNull
    private Integer clanId;

    @Id
    @NonNull
    private Integer userId;

    @NonNull
    private String userName;

    @NonNull
    private String userRoleName;

    @NonNull
    private String userRole;
}
