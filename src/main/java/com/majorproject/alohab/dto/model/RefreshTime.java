package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class RefreshTime {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer refreshId;

    @NonNull
    private String refreshName;

    @NonNull
    private LocalDate refreshDate;

    @NonNull
    private LocalTime refreshTime;
}
