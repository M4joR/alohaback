package com.majorproject.alohab.dto.model;


import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class DeletedUserNotification {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer notificationId;

    @NonNull
    private String deleteUserName;

    @NonNull
    private LocalDate date;

    @NonNull
    private LocalTime time;

    @NonNull
    private Boolean isAccepted;

    @NonNull
    private String owner;
}
