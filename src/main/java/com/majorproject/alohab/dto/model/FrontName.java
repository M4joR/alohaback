package com.majorproject.alohab.dto.model;

import lombok.*;

import javax.persistence.*;

@RequiredArgsConstructor
@NoArgsConstructor
@Entity
@Table
@Data
@ToString
public class FrontName {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    @NonNull
    String frontName;

    @NonNull
    String frontId;

    @NonNull
    Integer provincesCount;
}
