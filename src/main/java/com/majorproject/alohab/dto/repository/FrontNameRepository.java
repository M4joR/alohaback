package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.FrontName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FrontNameRepository extends JpaRepository<FrontName, Integer> {
    Optional<FrontName> findById(Integer id);
}
