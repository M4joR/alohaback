package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.RefreshTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RefreshTimeRepository extends JpaRepository<RefreshTime, Integer> {
    Optional<RefreshTime> findById(Integer id);
    RefreshTime findByRefreshName(String refreshName);
}
