package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, String> {
    Optional<Province> findById(String id);
    List<Province> findAllByOwnerClanId(Integer basicClanId);
    List<Province> findAllByAttackerContains(Integer basicClanId);
    List<Province> findAllByCompetitorContains(Integer basicClanId);
    List<Province> findDistinctByOwnerClanIdAndAttackerIsNotNullAndCompetitorIsNull(Integer basicClanId);
    List<Province> findDistinctByOwnerClanIdAndAttackerIsNullAndCompetitorIsNotNull(Integer basicClanId);
    List<Province> findDistinctByOwnerClanIdAndAttackerIsNotNullAndCompetitorIsNotNull(Integer basicClanId);
}
