package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.DeletedUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DeletedUserRepository extends JpaRepository<DeletedUser, Integer> {
    Optional<DeletedUser> findById(Integer id);
    void deleteByUserId(Integer userId);

    @Query(value = "SELECT u.userName FROM DeletedUser u WHERE u.userId = :userId ")
    String findUserNameByUserId(@Param("userId") Integer userId);
}
