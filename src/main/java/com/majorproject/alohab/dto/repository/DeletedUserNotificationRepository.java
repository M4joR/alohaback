package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.DeletedUserNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeletedUserNotificationRepository extends JpaRepository<DeletedUserNotification, Integer> {
    Optional<DeletedUserNotification> findById(Integer id);

    @Query(value = "SELECT * FROM deleted_user_notification n ORDER BY n.date DESC, n.time DESC LIMIT 100", nativeQuery = true)
    List<DeletedUserNotification> findAllNotifications();
}
