package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.ClanRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClanRatingRepository extends JpaRepository<ClanRating, Long> {
    Optional<ClanRating> findById(Long clanRatingId);

    @Query("SELECT cr FROM ClanRating cr ORDER BY cr.date DESC")
    List<ClanRating> findAllSort();
}
