package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.Absence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface AbsenceRepository extends JpaRepository<Absence, Long> {
    Optional<Absence> findById(Long absenceId);

    List<Absence> findAllByEndAbsenceIsGreaterThan(LocalDate endAbsence);

    void deleteAbsenceByAbsenceId(Long absenceId);
}
