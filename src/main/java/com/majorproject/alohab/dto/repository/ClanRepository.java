package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.Clan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClanRepository extends JpaRepository<Clan, Integer> {
    Optional<Clan> findById(Integer id);
    void deleteByClanId(Integer clanId);

    @Query(value = "SELECT u.clanId FROM Clan u")
    List<Integer> findAllClanId();

}
