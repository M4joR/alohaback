package com.majorproject.alohab.dto.repository;

import com.majorproject.alohab.dto.model.UserWithClan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserWithClanRepository extends JpaRepository<UserWithClan, Integer> {
    Optional<UserWithClan> findById(Integer id);
    List<UserWithClan> findByClanId(Integer clanId);
    List<UserWithClan> deleteByClanIdIn(List<Integer> clanId);

    @Query("SELECT u FROM UserWithClan u WHERE u.clanId IN (:clanIdList) AND u.userId NOT IN (:userIdList)")
    List<UserWithClan> findAllByClanIdAndNotInUserId(
            @Param("clanIdList") List<Integer> clanId,
            @Param("userIdList") List<Integer> userId
    );
}
