package com.majorproject.alohab.dto.repository;


import com.majorproject.alohab.dto.model.ClanMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClanMemberRepository extends JpaRepository<ClanMember, Integer> {
    Optional<ClanMember> findById(Integer userId);
    ClanMember findOneByUserId(Integer userId);
}
