package com.majorproject.alohab.dto.request;

import com.majorproject.alohab.dto.model.ClanMember;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class AbsenceRequest {
    private Integer clanMemberId;

    private LocalDate startAbsence;

    private LocalDate endAbsence;

    private String description;
}
