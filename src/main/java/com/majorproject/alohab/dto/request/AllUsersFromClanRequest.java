package com.majorproject.alohab.dto.request;

import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class AllUsersFromClanRequest {

    @NonNull
    private List<Integer> clanId;

    @NonNull
    private List<Integer> userId;

}
