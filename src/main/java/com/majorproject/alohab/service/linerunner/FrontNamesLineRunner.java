package com.majorproject.alohab.service.linerunner;

import com.google.gson.Gson;
import com.majorproject.alohab.dto.wargaming.Request;
import com.majorproject.alohab.dto.wargaming.globalmap.FrontNameDataModel;
import com.majorproject.alohab.service.FrontNameService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FrontNamesLineRunner implements CommandLineRunner {

    private final Request request;

    private final FrontNameService frontNameService;

    @Value("${applicationId}")
    private String applicationId;

    @Value("${basicClanId}")
    private String basicClanId;

    @Override
    public void run(String... args) throws Exception {
        if (frontNameService.getAllProvincesName().isEmpty()){
            FrontNameDataModel frontNameDataModel = new Gson().fromJson(getJson(), FrontNameDataModel.class);
            frontNameService.addFrontName(frontNameDataModel.getData());
        }
    }

    private String getJson() {
        return request.getResponse("https://api.worldoftanks.eu/wot/globalmap/fronts/?application_id="
                        +applicationId
                        +"&fields=front_name%2C+front_id%2C+provinces_count"
                        , "GET");
    }
}
