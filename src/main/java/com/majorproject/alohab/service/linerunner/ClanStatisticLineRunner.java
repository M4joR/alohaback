package com.majorproject.alohab.service.linerunner;

import com.google.gson.Gson;
import com.majorproject.alohab.dto.model.ClanRating;
import com.majorproject.alohab.dto.wargaming.Request;
import com.majorproject.alohab.dto.wargaming.clanstatistic.AdvancedEloRating;
import com.majorproject.alohab.dto.wargaming.clanstatistic.StatisticDatesDataModel;
import com.majorproject.alohab.dto.wargaming.clanstatistic.StatisticDetailsDataModel;
import com.majorproject.alohab.service.ClanRatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class ClanStatisticLineRunner implements CommandLineRunner {

    private final Request request;

    private final ClanRatingService clanRatingService;

    @Value("${applicationId}")
    private String applicationId;

    @Value("${basicClanId}")
    private String basicClanId;

    @Value("${firstTime}")
    private Boolean firstTime;

    @Override
    public void run(String... args) throws Exception {

        if (Boolean.TRUE.equals(firstTime)) {
            StatisticDatesDataModel statisticDatesDataModel = new Gson().fromJson(getDateJson(), StatisticDatesDataModel.class);
            statisticDatesDataModel.getData().getAll().getDates().forEach(date -> {
                StatisticDetailsDataModel statisticDetailsDataModel = new Gson().fromJson(getStatisticJson(date), StatisticDetailsDataModel.class);
                addStatisticToDatabase(statisticDetailsDataModel.getData(), date);
            });
        }
    }

    private void addStatisticToDatabase(Map<Integer, AdvancedEloRating> data, Integer date) {
        for (Map.Entry<Integer, AdvancedEloRating> entry : data.entrySet()) {
            clanRatingService.addRating(new ClanRating(
                    getLocalDateFromInteger(date),
                    entry.getValue().getFb_elo_rating_10().getRank(),
                    entry.getValue().getFb_elo_rating_10().getRank_delta(),
                    entry.getValue().getFb_elo_rating_10().getValue()
            ));
        }
    }

    private String getDateJson() {
        return
                request.getResponse("https://api.worldoftanks.eu/wot/clanratings/dates/?application_id="
                                + applicationId
                        , "GET");
    }

    private String getStatisticJson(Integer date) {
        return request.getResponse("https://api.worldoftanks.eu/wot/clanratings/clans/?application_id=" + applicationId +
                        "&clan_id=" + basicClanId +
                        "&fields=fb_elo_rating_10" +
                        "&date=" + date
                , "GET");
    }

    private LocalDate getLocalDateFromInteger(Integer date) {
        Instant instant = Instant.ofEpochSecond(date);
        long epoch = instant.toEpochMilli();
        return Instant.ofEpochMilli(epoch)
                .atZone(ZoneId.systemDefault()).toLocalDate();
    }
}
