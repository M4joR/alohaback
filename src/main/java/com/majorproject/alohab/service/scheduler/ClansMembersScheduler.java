package com.majorproject.alohab.service.scheduler;

import com.google.gson.Gson;
import com.majorproject.alohab.dto.model.DeletedUser;
import com.majorproject.alohab.dto.model.UserWithClan;
import com.majorproject.alohab.service.ClanService;
import com.majorproject.alohab.service.DeletedUserService;
import com.majorproject.alohab.service.RefreshTimeService;
import com.majorproject.alohab.service.UserWithClanService;
import com.majorproject.alohab.dto.wargaming.Request;
import com.majorproject.alohab.dto.wargaming.members.Members;
import com.majorproject.alohab.dto.wargaming.members.MembersDataModel;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ClansMembersScheduler {

    private final ClanService clanService;

    private final UserWithClanService userWithClanService;

    private final Request request;

    private final DeletedUserService deletedUserService;

    private final RefreshTimeService refreshTimesService;

    private static final String REFRESH_NAME = "RefreshDeletedMembers";

    @Value("${applicationId}")
    private String applicationId;

    @Scheduled(fixedDelay = 1000 * 60 * 30)
    public void updateList() {
        if (!clanService.getClanIds().isEmpty()) {
            getActualMembers();
            if (refreshTimesService.getTime(REFRESH_NAME) == null) {
                refreshTimesService.addTimes(REFRESH_NAME);
            } else {
                refreshTimesService.updateTimes(REFRESH_NAME);
            }
        }
    }

    private void getActualMembers() {
        MembersDataModel membersDataModel = new Gson().fromJson(getJson(), MembersDataModel.class);
        List<UserWithClan> users = mapMemberList(membersDataModel.getData());
        deletedUserService.addDeletedUserList(prepareDeletedUsers(userWithClanService.allDeletedUsersByClanId(clanService.getClanIds(), getUsersIds(users))));
        userWithClanService.deleteAllUsersByClanId(clanService.getClanIds());
        userWithClanService.addUserWithClanList(users);
    }

    private List<DeletedUser> prepareDeletedUsers(List<UserWithClan> allDeletedUsersByClanId) {
        List<DeletedUser> deletedUsersRequestList = new ArrayList<>();
        allDeletedUsersByClanId.forEach(userWithClan -> deletedUsersRequestList.add(new DeletedUser(
                userWithClan.getUserId(),
                userWithClan.getUserName(),
                userWithClan.getUserRoleName(),
                LocalDate.now(),
                userWithClan.getClanId()
        )));
        return deletedUsersRequestList;
    }

    private List<UserWithClan> mapMemberList(Map<Integer, Members> data) {
        List<UserWithClan> memberList = new ArrayList<>();
        for (Map.Entry<Integer, Members> entry : data.entrySet()) {
            entry.getValue().getMembers().forEach(el -> memberList.add(new UserWithClan(
                    entry.getKey(),
                    el.getAccount_id(),
                    el.getAccount_name(),
                    el.getRole_i18n(),
                    el.getRole()

            )));
        }

        return memberList;
    }

    private List<Integer> getUsersIds(List<UserWithClan> users) {
        List<Integer> usersIds = new ArrayList<>();
        users.forEach(user -> usersIds.add(user.getUserId()));
        return usersIds;
    }

    private String prepareIds(List<Integer> idList) {
        return idList.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
    }

    private String getJson() {
        return request.getResponse("https://api.worldoftanks.eu/wot/clans/info/?application_id="
                        + applicationId
                        + "&clan_id="
                        + prepareIds(clanService.getClanIds())
                        + "&fields=members.account_id%2C+members.account_name%2C+members.role_i18n%2C+members.role&language=pl",
                "GET");
    }
}
