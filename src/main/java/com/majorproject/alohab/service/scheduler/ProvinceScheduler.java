package com.majorproject.alohab.service.scheduler;

import com.google.gson.Gson;
import com.majorproject.alohab.dto.model.FrontName;
import com.majorproject.alohab.dto.wargaming.Request;
import com.majorproject.alohab.dto.wargaming.globalmap.ProvinceDataModel;
import com.majorproject.alohab.dto.wargaming.globalmap.Provinces;
import com.majorproject.alohab.service.FrontNameService;
import com.majorproject.alohab.service.ProvinceServices;
import com.majorproject.alohab.service.RefreshTimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ProvinceScheduler {

    private final FrontNameService frontNameService;

    private final ProvinceServices provinceServices;

    private final RefreshTimeService refreshTimesService;

    private static final String REFRESH_NAME = "RefreshProvinces";

    private final Request request;

    @Value("${applicationId}")
    private String applicationId;

    @Scheduled(fixedDelay = 1000 * 60 * 5 )
    public void updateProvinces(){
        frontNameService.getAllProvincesName().forEach(frontName -> {
            for(int i = 0; i <= (frontName.getProvincesCount()/100) - 1; i++){
                ProvinceDataModel provinceDataModel = new Gson().fromJson(getJson(frontName.getFrontId(), i+1), ProvinceDataModel.class);
                mapProvinces(provinceDataModel.getData(), frontName);
            }
        });
        if (refreshTimesService.getTime(REFRESH_NAME) == null) {
            refreshTimesService.addTimes(REFRESH_NAME);
        } else {
            refreshTimesService.updateTimes(REFRESH_NAME);
        }
    }

    private void mapProvinces(List<Provinces> data, FrontName frontName) {
        data.forEach(el -> provinceServices.updateData(el, frontName.getFrontName()));
    }

    private String getJson(String frontId, Integer page){
        return request.getResponse("https://api.worldoftanks.eu/wot/globalmap/provinces/?application_id=" + applicationId
                        +"&front_id=" +frontId
                        +"&fields=province_id%2Carena_name%2Carena_id%2Cprovince_name%2Cround_number%2Cattackers%2Ccompetitors%2Curi%2Cowner_clan_id%2Cprime_time%2Cdaily_revenue%2C&language=pl"
                        + "&page_no=" + page,
                "GET");
    }
}
