package com.majorproject.alohab.service.scheduler;

import com.google.gson.Gson;
import com.majorproject.alohab.dto.model.ClanMember;
import com.majorproject.alohab.dto.wargaming.members.Members;
import com.majorproject.alohab.dto.wargaming.members.MembersDataModel;
import com.majorproject.alohab.dto.wargaming.Request;
import com.majorproject.alohab.service.ClanMemberService;
import com.majorproject.alohab.service.RefreshTimeService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class BasicClanScheduler {

    private static final String REFRESH_NAME = "RefreshClanMembers";

    private final RefreshTimeService refreshTimesService;

    private final ClanMemberService clanMemberService;

    private final Request request;


    @Value("${basicClanId}")
    private String basicClanId;

    @Value("${applicationId}")
    private String applicationId;

    @Scheduled(fixedDelay = 1000 * 60 * 60 * 6)
    public void updateMemberList() {
        if (refreshTimesService.getTime(REFRESH_NAME) == null) {
            refreshTimesService.addTimes(REFRESH_NAME);
        } else {
            refreshTimesService.updateTimes(REFRESH_NAME);
        }

        clanMemberService.deleteMembers();
        getMembers();

    }

    private void getMembers() {
        MembersDataModel membersDataModel = new Gson().fromJson(getJson(), MembersDataModel.class);
        List<ClanMember> users = mapMemberList(membersDataModel.getData());
        clanMemberService.addMembers(users);
    }

    private List<ClanMember> mapMemberList(Map<Integer, Members> data) {
        List<ClanMember> memberList= new ArrayList<>();
        for(Map.Entry<Integer, Members> entry : data.entrySet()){
            entry.getValue().getMembers().forEach(el -> memberList.add(new ClanMember(
                    el.getAccount_id(),
                    el.getAccount_name(),
                    el.getRole_i18n(),
                    el.getRole()

            )));
        }

        return memberList;
    }

    private String getJson(){
        return request.getResponse("https://api.worldoftanks.eu/wot/clans/info/?application_id="
                        + applicationId
                        + "&clan_id="
                        + basicClanId
                        + "&fields=members.account_id%2C+members.account_name%2C+members.role_i18n%2C+members.role&language=pl",
                "GET");
    }
}
