package com.majorproject.alohab.service.scheduler;

import com.google.gson.Gson;
import com.majorproject.alohab.dto.model.ClanRating;
import com.majorproject.alohab.dto.wargaming.Request;
import com.majorproject.alohab.dto.wargaming.clanstatistic.AdvancedEloRating;
import com.majorproject.alohab.dto.wargaming.clanstatistic.StatisticDetailsDataModel;
import com.majorproject.alohab.service.ClanRatingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class ClanRatingScheduler {
    private final Request request;

    private final ClanRatingService clanRatingService;

    @Value("${applicationId}")
    private String applicationId;

    @Value("${basicClanId}")
    private String basicClanId;

    @Scheduled(cron = "0 0 7 * * *")
    public void updateClanRating() {
        updateStatistic();
    }

    private void updateStatistic() {
        StatisticDetailsDataModel statisticDetailsDataModel = new Gson().fromJson(getStatisticJson(), StatisticDetailsDataModel.class);
        addStatisticToDatabase(statisticDetailsDataModel.getData());
    }

    private void addStatisticToDatabase(Map<Integer, AdvancedEloRating> data) {
        for (Map.Entry<Integer, AdvancedEloRating> entry : data.entrySet()) {
            clanRatingService.addRating(new ClanRating(
                    LocalDate.now().minusDays(1),
                    entry.getValue().getFb_elo_rating_10().getRank(),
                    entry.getValue().getFb_elo_rating_10().getRank_delta(),
                    entry.getValue().getFb_elo_rating_10().getValue()
            ));
        }
    }

    private String getStatisticJson() {
        return
                request.getResponse("https://api.worldoftanks.eu/wot/clanratings/clans/?application_id=" + applicationId +
                                "&clan_id=" + basicClanId +
                                "&fields=fb_elo_rating_10"
                        , "GET");
    }
}
