package com.majorproject.alohab.service;

import com.majorproject.alohab.dto.model.UserWithClan;
import com.majorproject.alohab.dto.repository.UserWithClanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserWithClanService {
    private final UserWithClanRepository userWithClanRepository;

    public List<UserWithClan> addUserWithClanList(List<UserWithClan> request) {
        List<UserWithClan> list = new ArrayList<>();
        request.forEach(user -> list.add(
                new UserWithClan(
                        user.getClanId(),
                        user.getUserId(),
                        user.getUserName(),
                        user.getUserRoleName(),
                        user.getUserRole()
                ))
        );
        return userWithClanRepository.saveAll(list);
    }

    public List<UserWithClan> allMembersByClanId(Integer clanId) {
        return userWithClanRepository.findByClanId(clanId);
    }

    public List<UserWithClan> allDeletedUsersByClanId(List<Integer> clanId, List<Integer> userId) {
        return userWithClanRepository.findAllByClanIdAndNotInUserId(clanId, userId);
    }
    @Transactional
    public List<UserWithClan> deleteAllUsersByClanId(List<Integer> clanId) {
        return userWithClanRepository.deleteByClanIdIn(clanId);
    }
}
