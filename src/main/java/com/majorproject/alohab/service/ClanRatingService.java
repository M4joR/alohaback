package com.majorproject.alohab.service;


import com.majorproject.alohab.dto.model.ClanRating;
import com.majorproject.alohab.dto.repository.ClanRatingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClanRatingService {
    private final ClanRatingRepository clanRatingRepository;

    public List<ClanRating> getRatings() {
        return clanRatingRepository.findAllSort();
    }

    public ClanRating addRating(ClanRating request) {
        return clanRatingRepository.save(new ClanRating(
                request.getDate(),
                request.getRank(),
                request.getRankDelta(),
                request.getValue()
        ));
    }
}
