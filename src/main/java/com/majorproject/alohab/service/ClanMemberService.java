package com.majorproject.alohab.service;


import com.majorproject.alohab.dto.model.ClanMember;
import com.majorproject.alohab.dto.repository.ClanMemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClanMemberService {
    private final ClanMemberRepository clanMemberRepository;

    @Transactional
    public void deleteMembers(){
        clanMemberRepository.deleteAll();
    }

    public List<ClanMember> addMembers(List<ClanMember> clanMemberRequestList) {
        List<ClanMember> clanMemberList = new ArrayList<>();

        clanMemberRequestList.forEach(user-> clanMemberList.add(new ClanMember(
                user.getUserId(),
                user.getUserName(),
                user.getUserRoleName(),
                user.getUserRole()
        )));

        return clanMemberRepository.saveAll(clanMemberList);
    }

    public ClanMember findById(Integer userId){
        return clanMemberRepository.findOneByUserId(userId);
    }
}
