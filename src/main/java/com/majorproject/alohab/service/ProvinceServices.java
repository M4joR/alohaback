package com.majorproject.alohab.service;

import com.majorproject.alohab.dto.model.Province;
import com.majorproject.alohab.dto.wargaming.globalmap.Provinces;
import com.majorproject.alohab.dto.repository.ProvinceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProvinceServices {

    private final ProvinceRepository provinceRepository;

    @Value("${basicClanId}")
    private Integer basicClanId;

    public void updateData(Provinces province, String frontName) {
        provinceRepository.save(new Province(
                province.getProvince_id(),
                province.getArena_name(),
                province.getArena_id(),
                province.getProvince_name(),
                province.getOwner_clan_id(),
                LocalTime.parse(province.getPrime_time()).plusHours(2),
                province.getDaily_revenue(),
                province.getUri(),
                frontName,
                province.getRound_number(),
                province.getAttackers(),
                province.getCompetitors()
                ));
    }

    public List<Province> getBasicClanProvinces() {
        return provinceRepository.findAllByOwnerClanId(basicClanId);
    }

    public List<Province> getClanBattles() {
        List<Province> provinces = provinceRepository.findAllByAttackerContains(basicClanId);
        provinces.addAll(provinceRepository.findAllByCompetitorContains(basicClanId));
        return provinces;
    }

    public List<Province> getClanDefends() {
        List<Province> provinceList = provinceRepository.findDistinctByOwnerClanIdAndAttackerIsNotNullAndCompetitorIsNotNull(basicClanId);
        provinceList.addAll(provinceRepository.findDistinctByOwnerClanIdAndAttackerIsNotNullAndCompetitorIsNull(basicClanId));
        provinceList.addAll(provinceRepository.findDistinctByOwnerClanIdAndAttackerIsNullAndCompetitorIsNotNull(basicClanId));
        return provinceList;
    }
}
