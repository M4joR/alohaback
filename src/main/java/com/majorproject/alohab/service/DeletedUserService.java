package com.majorproject.alohab.service;


import com.majorproject.alohab.dto.model.DeletedUser;
import com.majorproject.alohab.dto.repository.DeletedUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DeletedUserService {
    private final DeletedUserRepository deletedUserRepository;

    public List<DeletedUser> addDeletedUserList(List<DeletedUser> request) {
        List<DeletedUser> list = new ArrayList<>();
        request.forEach(user -> list.add(
                new DeletedUser(
                        user.getUserId(),
                        user.getUserName(),
                        user.getUserRoleName(),
                        user.getDate(),
                        user.getClanId()
                ))
        );
        return deletedUserRepository.saveAll(list);
    }

    public List<DeletedUser> getAllDeletedUsers(){
        return deletedUserRepository.findAll();
    }

    public void deleteUser(Integer userId) {
        deletedUserRepository.deleteByUserId(userId);
    }

    public String getUserNameByUserId(Integer userId){return deletedUserRepository.findUserNameByUserId(userId); }
}
