package com.majorproject.alohab.service;

import com.majorproject.alohab.dto.model.DeletedUserNotification;
import com.majorproject.alohab.dto.repository.DeletedUserNotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DeletedUserNotificationService {
    private final DeletedUserNotificationRepository deletedUserNotificationRepository;

    public DeletedUserNotification addNotification(String deleteUserName, Boolean isAccepted, String owner){
        return deletedUserNotificationRepository.save(new DeletedUserNotification(
                deleteUserName,
                LocalDate.now(),
                LocalTime.now(),
                isAccepted,
                owner
        ));
    }

    public List<DeletedUserNotification> getAllNotifications(){
        return deletedUserNotificationRepository.findAllNotifications();
    }

}
