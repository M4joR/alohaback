package com.majorproject.alohab.service;

import com.majorproject.alohab.dto.model.Absence;
import com.majorproject.alohab.dto.repository.AbsenceRepository;
import com.majorproject.alohab.dto.request.AbsenceRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AbsenceService {
    private final AbsenceRepository absenceRepository;

    private final ClanMemberService clanMemberService;

    public List<Absence> findAllAbsence() {
        LocalDate localDate = LocalDate.now().minusDays(7);
        return absenceRepository.findAllByEndAbsenceIsGreaterThan(localDate);
    }

    public Absence addAbsence(AbsenceRequest absenceRequest) {
        return absenceRepository.save(
                new Absence(
                        clanMemberService.findById(absenceRequest.getClanMemberId()),
                        LocalDate.now(),
                        absenceRequest.getStartAbsence().plusDays(1),
                        absenceRequest.getEndAbsence().plusDays(1),
                        absenceRequest.getDescription()
                )
        );
    }

    @Transactional
    public void deleteAbsence(Long absenceId){
        absenceRepository.deleteAbsenceByAbsenceId(absenceId);
    }
}
