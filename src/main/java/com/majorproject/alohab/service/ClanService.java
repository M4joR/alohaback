package com.majorproject.alohab.service;


import com.majorproject.alohab.dto.model.Clan;
import com.majorproject.alohab.dto.repository.ClanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClanService {
    private final ClanRepository clanRepository;

    public List<Clan> getClans() {
        return clanRepository.findAll();
    }

    public Clan addClan(Clan clan) {
        return clanRepository.save(clan);
    }

    @Transactional
    public void deleteClan(Integer clanId) {
        clanRepository.deleteByClanId(clanId);
    }

    public List<Integer> getClanIds() {
        return clanRepository.findAllClanId();
    }

    public List<Clan> addClanList(List<Clan> clanlist) {
        return clanRepository.saveAll(clanlist);
    }
}
