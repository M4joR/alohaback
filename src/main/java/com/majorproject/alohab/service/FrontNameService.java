package com.majorproject.alohab.service;

import com.majorproject.alohab.dto.wargaming.globalmap.FrontDetails;
import com.majorproject.alohab.dto.model.FrontName;
import com.majorproject.alohab.dto.repository.FrontNameRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FrontNameService {
    private final FrontNameRepository frontNameRepository;

    public void addFrontName(List<FrontDetails> frontDetailsList) {
        frontDetailsList.forEach(frontDetails -> frontNameRepository.save(
                new FrontName(
                        frontDetails.getFront_name(),
                        frontDetails.getFront_id(),
                        frontDetails.getProvinces_count()
                )));
    }

    public List<FrontName> getAllProvincesName(){
        return frontNameRepository.findAll();
    }
}
