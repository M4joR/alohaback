package com.majorproject.alohab.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtService {

    private static final String SECRET_KEY = "WxXN)7,Ud7=t1+E";

    public final String generateToken(String userName, String accessToken, Integer accountId, Integer expiresAt, String userRole, String userRoleName, Integer userRoleId) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("accessToken", accessToken);
        claims.put("accountId",accountId);
        claims.put("userRole",userRole);
        claims.put("userRoleName",userRoleName);
        claims.put("userRoleId", userRoleId);
        return createToken(claims, userName, expiresAt);
    }

    private String createToken(Map<String, Object> claims, String userName, Integer expiresAt) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userName)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(expiresAt))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }
}
