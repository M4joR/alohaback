package com.majorproject.alohab.service;

import com.majorproject.alohab.dto.model.RefreshTime;
import com.majorproject.alohab.dto.repository.RefreshTimeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class RefreshTimeService {

    private final RefreshTimeRepository refreshTimesRepository;

    public RefreshTime updateTimes(String name){
        RefreshTime refreshTimes= refreshTimesRepository.findByRefreshName(name);
        refreshTimes.setRefreshName(name);
        refreshTimes.setRefreshTime(LocalTime.now());
        refreshTimes.setRefreshDate(LocalDate.now());
        return refreshTimesRepository.save(refreshTimes);
    }

    public RefreshTime addTimes(String name){
        return refreshTimesRepository.save(new RefreshTime(
           name,
           LocalDate.now(),
           LocalTime.now()
        ));
    }

    public RefreshTime getTime(String name){
        return refreshTimesRepository.findByRefreshName(name);
    }
    
    public List<RefreshTime> getAllTimes(){
        return refreshTimesRepository.findAll();
    }
}
